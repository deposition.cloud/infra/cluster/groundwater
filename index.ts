import * as pulumi from "@pulumi/pulumi"
import * as k8s from "@pulumi/kubernetes"

const HTTP = 80
const HTTPS = 443

let config = new pulumi.Config()

let domain = config.require("domain")
console.log(`Booting up stack for ${domain}...`)

/*
 * linkerd-viz
 */

const linkerdVizName = 'mesh-viz'
const linkerdVisNamespaceName = 'linkerd-viz'

const linkerdViz = new k8s.helm.v3.Chart(linkerdVizName, {
  repo: "linkerd",
  chart: "linkerd-viz",
  values: {}
}, {})

const webIngressAuthSecretName = 'web-ingress-auth'
const basicAuthSecret = new k8s.core.v1.Secret(webIngressAuthSecretName, {
  data: { auth: 'YWRtaW46JGFwcjEkZkouUEY2WVYkemJQNmtHVFBsTzN4dlIuMnlJNjVLLwo=' },
  metadata: { namespace: linkerdVisNamespaceName }
}, {
  parent: linkerdViz
})

const linkerdHost = `linkerd.${domain}`

const linkerdVizIngress = new k8s.networking.v1beta1.Ingress('ingress', {
  metadata: {
    namespace: linkerdVisNamespaceName,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      'nginx.ingress.kubernetes.io/upstream-vhost': '$service_name.$namespace.svc.cluster.local:8084',
      'nginx.ingress.kubernetes.io/configuration-snippet': `
        proxy_set_header Origin "";
        proxy_hide_header l5d-remote-ip;
        proxy_hide_header l5d-server-id;
      `,
      'nginx.ingress.kubernetes.io/auth-type': 'basic',
      'nginx.ingress.kubernetes.io/auth-secret': basicAuthSecret.metadata.name,
      'nginx.ingress.kubernetes.io/auth-realm': 'Authentication Required',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: [linkerdHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: linkerdHost,
      http: {
        paths: [{
          backend: { serviceName: 'web', servicePort: 8084 },
          path: '/',
          pathType: 'ImplementationSpecific'
        }]
      }
    }]
  }
}, {
  dependsOn: [linkerdViz]
})


/*
* grafana
*/

const grafanaName = 'view'
const grafanaNamespaceNamePrefix = 'grafana'
const grafanaNamespace = new k8s.core.v1.Namespace(grafanaNamespaceNamePrefix)
const grafanaNamespaceName = grafanaNamespace.metadata.name

const grafanaHost = `${grafanaNamespaceNamePrefix}.${domain}`

// TODO use introspection instead
const grafanaServiceName = `${grafanaName}-${grafanaNamespaceNamePrefix}`

const grafana = new k8s.helm.v3.Chart(grafanaName, {
  repo: "grafana",
  chart: "grafana",
  values: {
    namespaceOverride: grafanaNamespace.metadata.name
  }
}, {
  parent: grafanaNamespace
})

const grafanaIngress = new k8s.networking.v1beta1.Ingress('grafana', {
  metadata: {
    namespace: grafanaNamespaceName,
    annotations: {
      'kubernetes.io/ingress.class': 'nginx',
      // Add the following line (staging first for testing, then apply the prod issuer)
      'cert-manager.io/cluster-issuer': 'letsencrypt-staging' // 'letsencrypt-prod'
    }
  },
  spec: {
    tls: [{
      hosts: [grafanaHost],
      secretName: 'default-tls-secret'
    }],
    rules: [{
      host: grafanaHost,
      http: {
        paths: [{
          backend: { serviceName: grafanaServiceName, servicePort: HTTP },
          path: '/',
          pathType: 'Prefix'
        }]
      }
    }
    ]
  }
}, {
  parent: grafana
})

export const linkerdURL = `https://${linkerdHost}`
export const grafanaURL = `https://${grafanaHost}`
