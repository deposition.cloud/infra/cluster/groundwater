# Groundwater

> water beneath the earth's surface, often between saturated soil and rock, that supplies wells and springs

Observability, tracing and mesh services for personal clouds.

## Deploy

### Linkerd and viz

```bash
helm repo add smallstep https://smallstep.github.io/helm-charts/
helm repo update
```

``` bash
helm repo add linkerd https://helm.linkerd.io/stable
helm repo update
```

``` bash
helm repo add gitlab https://charts.gitlab.io
helm repo update
pulumi config set --secret runnerRegistrationToken <value from https://gitlab.com/groups/deposition.cloud/-/settings/ci_cd>
```

Install [GitLab Runner Helm Chart](https://docs.gitlab.com/runner/install/kubernetes.html)

``` bash
helm repo add grafana https://grafana.github.io/helm-charts
helm repo update
```

Get the admin password to log into [grafana.deposition.cloud](https://grafana.deposition.cloud)

``` bash
kubectl get secret --namespace grafana-7sb8q05f view-grafana -o jsonpath="{.data.admin-password}" |
base64 --decode ; echo
```

### Linkerd Multicluster

> TODO: traffic split between live and omega for failover scenarios

#### Step Certificates

``` bash
cd ca
step-cli certificate create root.linkerd.cluster.local ca.crt ca.key --profile root-ca --no-password --insecure
step-cli certificate create identity.linkerd.cluster.local issuer.crt issuer.key --profile intermediate-ca --not-after 8760h --no-password --insecure --ca ca.crt --ca-key ca.key
```

## References

- [Linkerd Generating your own mTLS root certificates](https://linkerd.io/2/tasks/generate-certificates/)
